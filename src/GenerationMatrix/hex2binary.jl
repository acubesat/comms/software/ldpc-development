using Match

function hex2binary(str)
    if length(str)< 3 || str[1] != '0' || str[2] != 'x'
        error("Hex strings must start wit 0x")
    end
    binaryLength = 4*length(str[3:end])
    binaryRepresentation = BitArray(undef, 1, binaryLength)
    j = 2
    for i = 1:4:binaryLength
        j +=1
        binaryRepresentation[i:i+3] = hex2bit(str[j])
    end
    return binaryRepresentation
end

function hex2bit(str)
    @match str begin
        '0' => BitArray([0 0 0 0])
        '1' => BitArray([0 0 0 1])
        '2' => BitArray([0 0 1 0])
        '3' => BitArray([0 0 1 1])
        '4' => BitArray([0 1 0 0])
        '5' => BitArray([0 1 0 1])
        '6' => BitArray([0 1 1 0])
        '7' => BitArray([0 1 1 1])
        '8' => BitArray([1 0 0 0])
        '9' => BitArray([1 0 0 1])
        'A' => BitArray([1 0 1 0])
        'B' => BitArray([1 0 1 1])
        'C' => BitArray([1 1 0 0])
        'D' => BitArray([1 1 0 1])
        'E' => BitArray([1 1 1 0])
        'F' => BitArray([1 1 1 1])
        _   => error("Not acceptable input")
    end
end
hex2binary("0x0A")